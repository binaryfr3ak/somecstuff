/********************************************************************************
*	Author:		Florian Bär
*
*	Description:	This software is used to get a perfect understanding
*			of the boolsche arithmetic in C and in other Languages.
*
*	Licence:	GNU General Public License
*
*	Development:
*	Time:	Name:	Date:		Step:
*	30min	BAERF	05.03.14	Refactoring Main, Insert Header
*
*
*********************************************************************************/
/*
*	INCLUDE SECTIONS
*/
#include<stdio.h>


/*
*	FUNCTION DECLARATION
*/
void Header();
void DoDecisionHeader();
int And();

// char* ConvertIntToBool(int valueToConvert);


/*
*	Global variable declaration
*	Normally, its not recommended to use them...
*	But here, it makes a lot much easier.
*/
int a = 1;
int b = 0;

/*
*
*	Main Function :)
*/

int main()
{
	char selectedKey = 'Q';

	do
	{
		Header();

		DoDecisionHeader();

		printf("\n\n");
		selectedKey = getchar();
        flush();
		// only for dev
		printf("Selected key %c", selectedKey);

		switch(selectedKey)
		{
			case 'a':
			case 'A':
				And();
				printf("Hit A");
				break;
			case 'Q':
			case 'q':
				// Ignore - action will be at the end of the loop
				printf("Hit Q");
				break;
			default:
				printf("Your input is invalid, try it again ;)");
				break;
		}


	} while (selectedKey != 'q' && selectedKey != 'Q');

	printf("\n\n");
	return 0;
}

void Header()
{
	printf("\n***************************************************************\n");
	printf("This tool will show you how the boolsche arithmetic in c works.\n");
	printf("***************************************************************\n\n");

	printf("NOTE:\nIn C, there are no booleans which represents a true or a false.\n\n");
	printf("There are only integers. And the values are as following:\n");

	printf("TRUE\t=> %i\nFALSE\t=> %i\n\n", a, b);

	printf("In this software are the variable a and b used.\n\n");
	printf("a has the value TRUE\nb has the value FALSE\n\n");

	printf("a = true = %i\nb = false = %i\n\n", a, b);
}

void DoDecisionHeader()
{
	printf("Show me what i should do as next:\n");
	printf("Q/q => Quit");
}

int And()
{
	printf("Here, i will show you, how the 'AND' works.\n");
	printf("It will written as a double '&' character.\n\n");
	printf("Example:\n");
	printf("\ta = 1 // true\n");
	printf("\tb = 0 // false\n");
	printf("\n");
	printf("\n");

}

/* Wait until i have more knowledge about returning char arrays from functions :)

char* ConvertIntToBool(int valueToConvert)
{
	char true[] = "true";
	char false[] = "false";

	if(valueToConvert)
	{
		return &true;
	}
	else
	{
		return &false;
	}
}*/
