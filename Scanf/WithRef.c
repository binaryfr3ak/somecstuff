/*
* This is a simple and stupid c application.
*/

#include<stdio.h>

int main()
{
	char name[100];
	int number, modulo;
	printf("Type in your name:\n");
	scanf("%s", name); // Does the compiler insert the ref?
	printf("\n\n");
	printf("Your name is %s", name);
	printf("\n\n");
	printf("Tell me your preferred number (no floating number):\n");
	scanf("%i", &number);
	printf("\nYour preferred number is %i.\n", number);
	printf("Calulating the modulo 2 from %i.", number);	
	modulo = number%2;
	printf("\nThe modulo of 2 is => %i.\n", modulo);
	return 0;
}
